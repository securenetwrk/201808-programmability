# Basic pod info

![](assets/images/dne-dci-dcloud.png)

| Hostname | Description | IP Address | Credentials |
| --- | --- | --- | --- |
| **wkst1** | Dev Workstation: Windows | 198.18.133.36 | dcloud\demouser/C1sco12345 |
| **ubuntu** | Dev Workstation: Linux | 198.18.134.28 | cisco/C1sco12345 |
| **nxosv-1** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.140 | admin/C1sco12345 |
| **nxosv-2** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.141 | admin/C1sco12345 |
| **apic1** | ACI Simulator - 2.1.1h | 198.18.133.200 | admin/C1sco12345 |
| **centos1** | CentOS 7 Server - Automation Target | 198.18.134.49 | root/C1sco12345 or demouser/C1sco12345 |
| **centos2** | CentOS 7 Server - Automation Target | 198.18.134.50 | root/C1sco12345 or demouser/C1sco12345 |
| ucsctl1 | UCS Central - 1.5(1b) | 198.18.133.90 | admin/C1sco12345 |
| ucsm1 | UCS Manager Emulator | 198.18.133.91 | admin/C1sco12345 |
| ucsd | UCS Director - 6.0.1.1 | 198.18.133.112 | admin/C1sco12345 |
| win2012r2 | Windows 2012 Standard Server - Automation Target | 198.18.133.20 | dcloud\administrator/C1sco12345 |
| vc1 | vCenter 6.0 | 198.18.133.30 | administrator@vsphere.local/C1sco12345! |
| vesx1 | vSphere Host | 198.18.133.31 | root/C1sco12345 |
| vesx2 | vSphere Host | 198.18.133.32 | root/C1sco12345 |
| cimc1 | UCS IMC Emulator | 198.18.134.88 | root/C1sco12345 or admin/C1sco12345 |
| na-edge1 | vNetApp | 198.18.133.115 | root/C1sco12345 |
| ad1 | Domain Controller | 198.18.133.1 | administrator/C1sco12345 |
| ios-xe-mgmt.cisco.com | CSR1000v - 16.08.01 | ios-xe-mgmt.cisco.com:8181 | root/D_Vay!_10& |


## Getting connected

1. Run Anyconnect client and fill in url with ```dcloud-sjc-anyconnect.cisco.com``` 

1. Click connect, and log in with the credentials from your assigned pod, listed below:

| Session Id |  Session Name |  Usernames |  Password |  Public IPs   | Pod URL |
| --- | --- | --- | --- | --- | --- |
| 99069 | 1 - Cisco DevNet Express Data Center v2 | v984user1; v984user2; ... v984user16 | e2e319 | 128.107.222.131 | https://v984user1:e2e319@128.107.222.131:8443 |
| 99068 | 2 - Cisco DevNet Express Data Center v2 | v698user1; v698user2; ... v698user16 | 59ebe9 | 128.107.222.132 | https://v698user1:59ebe9@128.107.222.132:8443 |
| 99067 | 3 - Cisco DevNet Express Data Center v2 | v25user1; v25user2; ... v25user16 | ebcf7e | 128.107.222.134 | https://v25user1:ebcf7e@128.107.222.134:8443 |
| 99066 | 4 - Cisco DevNet Express Data Center v2 | v739user1; v739user2; ... v739user16 | a6c41d | 128.107.222.135 | https://v739user1:a6c41d@128.107.222.135:8443 |
| 99065 | 5 - Cisco DevNet Express Data Center v2 | v331user1; v331user2; ... v331user16 | 219593 | 128.107.222.136 | https://v331user1:219593@128.107.222.136:8443 |
| 99064 | 6 - Cisco DevNet Express Data Center v2 | v70user1; v70user2; ... v70user16 | 9581ac | 128.107.222.137 | https://v70user1:9581ac@128.107.222.137:8443 |
| 99063 | 7 - Cisco DevNet Express Data Center v2 | v591user1; v591user2; ... v591user16 | 994185 | 128.107.222.143 | https://v591user1:994185@128.107.222.143:8443 |
| 99062 | 8 - Cisco DevNet Express Data Center v2 | v474user1; v474user2; ... v474user16 | 705054 | 128.107.222.144 | https://v474user1:705054@128.107.222.144:8443 |
| 99061 | 9 - Cisco DevNet Express Data Center v2 | v810user1; v810user2; ... v810user16 | e8464e | 128.107.222.37 | https://v810user1:e8464e@128.107.222.37:8443 |
| 99060 | 10 - Cisco DevNet Express Data Center v2 | v1212user1; v1212user2; ... v1212user16 | 148ac1 | 128.107.222.145 | https://v1212user1:148ac1@128.107.222.145:8443 |
| 99059 | 11 - Cisco DevNet Express Data Center v2 | v208user1; v208user2; ... v208user16 | 8ec668 | 128.107.222.146 | https://v208user1:8ec668@128.107.222.146:8443 |
| 99058 | 12 - Cisco DevNet Express Data Center v2 | v521user1; v521user2; ... v521user16 | acde2a | 128.107.222.147 | https://v521user1:acde2a@128.107.222.147:8443 |
| 99057 | 13 - Cisco DevNet Express Data Center v2 | v965user1; v965user2; ... v965user16 | 01ada6 | 128.107.222.148 | https://v965user1:01ada6@128.107.222.148:8443 |
| 99056 | 14 - Cisco DevNet Express Data Center v2 | v749user1; v749user2; ... v749user16 | f6f4b5 | 128.107.222.149 | https://v749user1:f6f4b5@128.107.222.149:8443 |
| 99055 | 15 - Cisco DevNet Express Data Center v2 | v1078user1; v1078user2; ... v1078user16 | ee423e | 128.107.222.151 | https://v1078user1:ee423e@128.107.222.151:8443 |
| 99054 | 16 - Cisco DevNet Express Data Center v2 | v752user1; v752user2; ... v752user16 | 9ad5f0 | 128.107.222.152 | https://v752user1:9ad5f0@128.107.222.152:8443 |
| 99053 | 17 - Cisco DevNet Express Data Center v2 | v924user1; v924user2; ... v924user16 | 8448e2 | 128.107.222.153 | https://v924user1:8448e2@128.107.222.153:8443 |
| 99052 | 18 - Cisco DevNet Express Data Center v2 | v599user1; v599user2; ... v599user16 | 648a0d | 128.107.222.155 | https://v599user1:648a0d@128.107.222.155:8443 |
| 99051 | 19 - Cisco DevNet Express Data Center v2 | v1105user1; v1105user2; ... v1105user16 | d2cd07 | 128.107.222.156 | https://v1105user1:d2cd07@128.107.222.156:8443 |
| 99050 | 20 - Cisco DevNet Express Data Center v2 | v1168user1; v1168user2; ... v1168user16 | 0ad854 | 128.107.222.157 | https://v1168user1:0ad854@128.107.222.157:8443 |
| 99049 | 21 - Cisco DevNet Express Data Center v2 | v245user1; v245user2; ... v245user16 | 3d8c9f | 128.107.222.158 | https://v245user1:3d8c9f@128.107.222.158:8443 |
| 99048 | 22 - Cisco DevNet Express Data Center v2 | v915user1; v915user2; ... v915user16 | bcc694 | 128.107.222.159 | https://v915user1:bcc694@128.107.222.159:8443 |
| 99047 | 23 - Cisco DevNet Express Data Center v2 | v257user1; v257user2; ... v257user16 | 0e8399 | 128.107.222.161 | https://v257user1:0e8399@128.107.222.161:8443 |
| 99046 | 24 - Cisco DevNet Express Data Center v2 | v576user1; v576user2; ... v576user16 | 12f36e | 128.107.222.166 | https://v576user1:12f36e@128.107.222.166:8443 |
| 99045 | 25 - Cisco DevNet Express Data Center v2 | v1460user1; v1460user2; ... v1460user16 | f3f59e | 128.107.222.174 | https://v1460user1:f3f59e@128.107.222.174:8443 |
| 99044 | 26 - Cisco DevNet Express Data Center v2 | v1140user1; v1140user2; ... v1140user16 | 801873 | 128.107.222.175 | https://v1140user1:801873@128.107.222.175:8443 |
| 99043 | 27 - Cisco DevNet Express Data Center v2 | v1410user1; v1410user2; ... v1410user16 | c95032 | 128.107.222.176 | https://v1410user1:c95032@128.107.222.176:8443 |
| 99042 | 28 - Cisco DevNet Express Data Center v2 | v1479user1; v1479user2; ... v1479user16 | 0a3c22 | 128.107.222.115 | https://v1479user1:0a3c22@128.107.222.115:8443 |
| 99041 | 29 - Cisco DevNet Express Data Center v2 | v823user1; v823user2; ... v823user16 | 2071d2 | 128.107.222.177 | https://v823user1:2071d2@128.107.222.177:8443 |
| 99040 | 30 - Cisco DevNet Express Data Center v2 | v759user1; v759user2; ... v759user16 | 0a4a25 | 128.107.222.178 | https://v759user1:0a4a25@128.107.222.178:8443 |



