# Summary

In this lab, we've explored the NETCONF module available with Ansible. Hopefully you already learned about NETCONF in the prior Model Driven Programming module, so now you can see that Ansible is an easy way to push configs using the NETCONF module. In the lab, we also showed that the NETCONF module is able to do similar configuration tasks as in the last lab where we used the native IOS modules, but this time using a more device-agnostic data model to describe the desired state.  

* Before moving to the next Lab, please execute the following playbook to clean up the changes your lab made to the shared sandbox router :

    ```bash
    ansible-playbook ansible-03-netconf-config/03-lab_cleanup.yaml
    ```
