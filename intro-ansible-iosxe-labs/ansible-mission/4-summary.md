## Summary
Excellent job! You are well on the way to being an Ansible master.  

### See the solution

If you'd like to see the solution to the mission, you can view it on [GitHub](https://github.com/CiscoDevNet/dnav3-code/tree/solutions/intro-ansible/ansible-04-mission).


* Before wrapping up, please execute the following playbook to clean up the changes your Mission made to the shared sandbox router :

    ```bash
    ansible-playbook ansible-04-mission/04-lab_cleanup.yaml
    ```
