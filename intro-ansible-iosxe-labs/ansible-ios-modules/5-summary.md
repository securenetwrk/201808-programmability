# Summary

In this Learning Lab we've explored the native IOS modules available with Ansible. We used several of the native modules to gather information about the routers, as well as pushing new configuration out to those devices. When reviewing the playbooks, you were able to see that each task is laid out in a human-readable manner, making ongoing support of the playbooks much easier even for people not familiar with Ansible or even IOS CLI. 

* Before moving to the next Lab, please execute the following playbook to clean up the changes your lab made to the shared sandbox router :

    ```bash
    ansible-playbook ansible-02-ios-modules/02-lab_cleanup.yaml
    ```
Proceed to the following Learning Lab: Ansible netconf modules with IOS XE routers
