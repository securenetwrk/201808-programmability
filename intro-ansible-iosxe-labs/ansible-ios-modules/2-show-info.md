# Step 1: Show device information

## Hands-on task:

1. Open a terminal, change to the root of the code samples repository, and make sure your virtual environment is active.
1. Change into the directory for this lab.  

    ```bash
    cd intro-ansible/
    ```

1. Within the directory is a folder called `ansible-02-ios-modules` that contain several sample playbooks.

    ```bash
    ls ansible-02-ios-modules
    ```

    <details>
    <summary> Expected Output </summary>
    <pre><code>
    02-ios_command_show.yaml
    02-ios_command_show_ntp.yaml
    02-lab_cleanup.yaml
    02-loopback_create.yaml
    02-loopback_delete.yaml
    02-ntp_create.yaml
    02-ntp_delete.yaml
    </code></pre>
    </details>

1. Within the directory is a file called `hosts` that contain both the device inventory, as well as a few key variables.

    ```bash
    cat hosts
    ```

    <details>
    <summary> Expected Output </summary>
    <pre><code>
    [all:vars]
    ansible_python_interpreter="/usr/bin/env python"

    [iosxe:vars]
    # Fill in a unique pod name with no spaces
    user_pod = mypod
    # Fill in a pod number between 10 and 250
    pod_number = 10
    # Fill in your Webex Teams auth token from https://developer.webex.com/getting-started.html
    webex_token = mytokenabcdef
    # Fill in your Webex Teams room name you wish to post to
    webex_room_id = roomIDabc123456defghi789jklmnopqr0

    [iosxe:children]
    # FYI - If you are in a DevNet Express please add a #
    # in front of sandbox and remove the # in front of express.
    sandbox
    #express

    [sandbox]
    ios-xe-mgmt.cisco.com ansible_port=8181

    [express]
    198.18.134.11  # dcloud pod router #1
    198.18.134.12  # dcloud pod router #2 
    </code></pre>
    </details>

1. Edit the hosts file and fill in `user_pod` with a string for your pod name, and a number for your `pod_number` between 10 and 250.

1. Save and exit the hosts file.

1. Execute the following playbook to check various show outputs:

    ```bash
    ansible-playbook ansible-02-ios-modules/02-ios_command_show.yaml
    ```

    <details>
    <summary> Sample Output </summary>
    <pre><code>
    TASK [GATHERING FACTS] *******************************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com]

    TASK [display current IOS version] *******************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com] => {
        "ansible_net_version": "16.08.01"
    }

    TASK [run show ip int brie] **************************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com]

    TASK [display value of "myint" variable *************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com] => {
        "myint[\"stdout_lines\"]": [
                "Interface              IP-Address      OK? Method Status                Protocol",
                "GigabitEthernet1       10.10.20.48     YES NVRAM  up                    up      ",
                "GigabitEthernet2       10.255.255.1    YES other  up                    up      ",
                "GigabitEthernet3       unassigned      YES unset  up                    up      ",
                "Port-channel2          unassigned      YES unset  down                  down"
        ]
    }

    TASK [run show users] ********************************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com]

    TASK [display value of "shuser" variable] ************************************************************************************************************
    ok: [ios-xe-mgmt.cisco.com] => {
        "shuser[\"stdout_lines\"]": [
                "Line       User       Host(s)              Idle       Location",
                "*  1 vty 0     root       idle                 00:00:00 10.24.12.232",
                "",
                "  Interface    User               Mode         Idle     Peer Address",
                "  unknown      a(ONEP)            com.cisco.sy 00:00:20 ",
                "  unknown      NETCONF(ONEP)      com.cisco.ne 00:00:20"
        ]
    }

    PLAY RECAP *******************************************************************************************************************************************
    ios-xe-mgmt.cisco.com      : ok=6    changed=0    unreachable=0    failed=0
    </code></pre>
    </details>

1. Observe in the output of the `ios_facts` task that we get a very specific variable value back, whereas with the `ios_command` tasks we get the equivalent of CLI output back. While that data could be further parsed to extract what we want, it is generally preferable to use more specific modules if they are available, and reserve general modules like `ios_command` and `ios_config` for scenarios where there is no more specific module available.
     * Note: If you see existing loopbacks defined as Loopback `pod_number` or Loopback `1pod_number` (for example, `Lo10` or `110` if your pod number is `10`), this means someone has previously used your pod number for a lab. We suggest choosing a new number in your hosts file to avoid confusion.

1. Open up `ansible-02-ios-modules/02-ios_command_show.yaml` in a text editor and review the contents.
 
 * Are you able to identify the module that is used for each task?
    <details>
    <summary> Answer </summary>
    This playbook leverages `ios_facts` and `ios_command` modules.
    </details>

 * Can you identify how the responses for the `ios_command` module are captured and then displayed?
    <details>
    <summary> Answer </summary>
    In each task that calls `ios_command`, the output of the command is registered to a variable with the `register:` parameter. For example, `register: myint` stores the output of one task in the `myint` variable.
    </details>

 * How is that different than how the version from `ios_facts` is displayed?
    <details>
    <summary> Answer </summary>
    `ios_facts` gathers common attributes and stores them directly into variables such as `ansible_net_version`. In contrast, `ios_command` returns the output from the command without parsing it into key-value pairs or variables.
    </details>

## Next step

Proceed to Step 2: [Define an NTP server.](3-create-ntp.md)
