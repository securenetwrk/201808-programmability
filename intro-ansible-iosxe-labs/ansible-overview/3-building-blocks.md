# Step 3: Understand Ansible building blocks
Ansible utilizes a number of common components that are important to understand. In this section we will review the basic ones.

## Config file

## Inventory files
Ansible leverages inventory files to define all hosts that will be managed by the control workstation. When executing Ansible, it will look at the config file and command line to determine which inventory file to use for that run. Subsequent runs can leverage different inventory files, allowing users to break large infrastructure environments into smaller batches easily.

### Host definition
Within the inventory file, there are two primary components. The first is host definitions. Host definitions are used to define each individual host that will be managed as part of that inventory.
```
198.18.134.11  # dcloud pod router #1
198.18.134.12  # dcloud pod router #2
```
In this example, we have defined 2 hosts, and put an optional description of each in a comment at the end of the line

### Group definition
Ansible makes use of groups to simplify management of infrastructure. Within an inventory file, users can define hosts within one or more groups.
```
[dc1-routers]
198.18.134.11  # dcloud pod router #1
198.18.134.12  # dcloud pod router #2
```
In this example, we have grouped the two routers into a group named `[dc1-routers]` as denoted by the square brackets. By creating a group, playbooks can now be run against multiple hosts easily by referencing the group name.

### Nested groups
Ansible also supports nesting groups to allow a more flexible approach to inventory definition.
```
[datacenter1:children]
dc1-routers
dc1-switches

[dc1-routers]
198.18.134.11  # dcloud pod router #1
198.18.134.12  # dcloud pod router #2

[dc1-switches]
198.18.134.13  # dcloud pod switch #1
```
In the above example, we have created a group `datacenter1` which contains routers in group dc1-routers as well as switches in dc1-switches. We can now reference the datacenter1 group when running playbooks instead of just using the switch or router group.

### Host ranges
Ansible allows for grouping of similar hosts using a range option in the inventory file.

```
[dc1-routers]
198.18.134.[11:19]  # dcloud pod router #1 - 9
```
In this example we define a range of similar devices without explicitly listing each out in the inventory file.


## Variables
Ansible can leverage variables when executing plays to make playbooks more scalable and flexible. Those variables can be defined in a number of locations, including in the inventory file, in files based on group or host identity, and even incline as part of a play.

## Inventory file
```
[iosxe:vars]
user_pod = <unique_name>
```
In our lab inventory file you will change the above sample to a value unique to you such as:
```
[iosxe:vars]
user_pod = my_cool_pod
```
By doing so, when you run the labs and mission your plays will use that variable to create unique components such as a vrf named `vrf_my_cool_pod`.

## group_vars file
Within the group_vars folder is a file named `iosxe.yml` with contents similar to:
```yaml
ansible_connection: network_cli
ansible_become: yes
ansible_become_method: enable
ansible_network_os: ios
```
This is a group of variables that will apply to members of the group `[iosxe]`.

## Next step

Proceed to Step 3: Understand a sample playbook structure.
